import face_recognition
import cv2

face1 = "/your/path/image.jpeg"
face2 = "/your/path/image2.jpeg"
#face3 = "/your/path/image3.jpeg"

immagine1 = face_recognition.load_image_file(face1)
immagine2 = face_recognition.load_image_file(face2)
volto1 = face_recognition.face_encodings(immagine1)[0]
volto2 = face_recognition.face_encodings(immagine2)[0]
#volto3 = face_recognition.face_encodings(immagine3)[0]
volte_con_nome = [
    (volto1, "face1"),
    (volto2, "face2"),
    #(volto3, "face3")
]

video_capture = cv2.VideoCapture(0)
while True:

    ret, frame = video_capture.read()
    volti_presenti = face_recognition.face_locations(frame)
    volti_presenti_codificati = face_recognition.face_encodings(frame, volti_presenti)
    for (top, right, bottom, left), volto_codificato in zip(volti_presenti, volti_presenti_codificati):
        nome_trovato = "Sconosciuto"

        for volto_con_nome in volte_con_nome:
            volto_conosciuto = volto_con_nome[0]
            nome_conosciuto = volto_con_nome[1]

            confronto = face_recognition.compare_faces([volto_conosciuto], volto_codificato)

            if confronto[0]:
                nome_trovato = nome_conosciuto
                break

        cv2.rectangle(frame, (left, top), (right, bottom), (0, 120, 255), 2)
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 100, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(frame, nome_trovato, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    cv2.imshow('Face Recognition with Video from Webcam', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()
