import cv2
import mediapipe as mp
import subprocess

mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands

def get_bbox_coordinates(results):
    bboxes = []
    for hand_landmarks in results.multi_hand_landmarks:
        x_min, x_max, y_min, y_max = 1, 0, 1, 0
        for landmark in hand_landmarks.landmark:
            x, y, z = landmark.x, landmark.y, landmark.z
            x_min, x_max = min(x, x_min), max(x, x_max)
            y_min, y_max = min(y, y_min), max(y, y_max)
        bboxes.append((x_min, x_max, y_min, y_max))
    return bboxes

cap = cv2.VideoCapture(0)

with mp_hands.Hands(
        min_detection_confidence=0.8,
        min_tracking_confidence=0.8) as hands:

    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Fallita lettura del gesto!")
            break

        image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
        image.flags.writeable = False
        results = hands.process(image)
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                if hand_landmarks.landmark[4].x < hand_landmarks.landmark[3].x:
                    cv2.putText(image, 'MI PIACE', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 100, 0), 2, cv2.LINE_AA)
                    #subprocess.Popen("/home/nicoz/Desktop/bash_scripting/apt_bash.sh", shell=True)
                    
                else:
                    cv2.putText(image, 'CIAO', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 120, 0), 2, cv2.LINE_AA)
                    #INSERISCI UN' AZIONE DA FARE QUANDO SI VERIFICA LA CONDIZIONE
                for hand_landmarks in results.multi_hand_landmarks:
                #determina la posizione del pollice e del mignolo
                    thumb_x = hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_TIP].x
                    thumb_y = hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_TIP].y
                    pinky_x = hand_landmarks.landmark[mp_hands.HandLandmark.PINKY_TIP].x
                    pinky_y = hand_landmarks.landmark[mp_hands.HandLandmark.PINKY_TIP].y

                #calcola la distanza tra il pollice e il mignolo
                    distance = ((thumb_x - pinky_x) ** 2 + (thumb_y - pinky_y) ** 2) ** 0.5

                #se la distanza è inferiore a una certa soglia, vuol dire che la mano è in posizione "a cuppetiello"
                    if distance < 0.1:
                    #scrivi "HELLO" nell'immagine
                        cv2.putText(image, "MA CHE DICI?!?!", (int(thumb_x * image.shape[1]), int(thumb_y * image.shape[0])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)


        # Estrazione dei punti chiave della mano
                    landmarks = results.multi_hand_landmarks[0].landmark

        # Estrazione della posizione del pollice e della punta dell'indice
                    thumb = landmarks[4]
                    index = landmarks[8]

        # Calcolo della distanza tra il pollice e la punta dell'indice
                    distance = ((thumb.x - index.x) ** 2 + (thumb.y - index.y) ** 2) ** 0.5

        # Verifica se la distanza è inferiore alla soglia di 0.1 (gesto OK)
                    if distance < 0.1:
                        print("OK")
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image, hand_landmarks, mp_hands.HAND_CONNECTIONS)

            bboxes = get_bbox_coordinates(results)
            for bbox in bboxes:
                cv2.rectangle(image, (int(bbox[0]*image.shape[1]), int(bbox[2]*image.shape[0])),
                              (int(bbox[1]*image.shape[1]), int(bbox[3]*image.shape[0])), (0, 100, 50), 2)

        cv2.imshow('U cuppetiell!', image)

        if cv2.waitKey(5) & 0xFF == 27:
            break

cap.release()
cv2.destroyAllWindows()


