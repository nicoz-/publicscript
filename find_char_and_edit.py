with open('mio_file.txt', 'r') as f:
    content = f.read()
    new_content = ""
    counter = 0

    for char in content:
        if char == ",":
            counter += 1
            if counter % 6 == 0:
                char = ",\n"
        new_content += char

with open("file.txt", "w") as f:
    f.write(new_content)
