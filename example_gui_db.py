import sys
import sqlite3
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QListWidget

class DatabaseApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Applicazione con Database")
        self.setGeometry(100, 100, 600, 400)

        # Connessione al database 
        self.conn = sqlite3.connect("mydatabase.db")
        self.cursor = self.conn.cursor()

        # creazione della tabella se non esiste
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS data (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                nome TEXT,
                cognome TEXT
            )
        """)
        self.conn.commit()

        self.init_ui()

    def init_ui(self):
        # creazione dei widget dell'interfaccia utente
        self.input_nome = QLineEdit()
        self.input_cognome = QLineEdit()
        self.btn_inserisci = QPushButton("Inserisci")
        self.btn_visualizza = QPushButton("Visualizza")
        self.btn_cerca = QPushButton("Cerca")

        self.result_list = QListWidget()

        # layout
        input_layout = QVBoxLayout()
        input_layout.addWidget(QLabel("Nome:"))
        input_layout.addWidget(self.input_nome)
        input_layout.addWidget(QLabel("Cognome:"))
        input_layout.addWidget(self.input_cognome)
        input_layout.addWidget(self.btn_inserisci)
        input_layout.addWidget(self.btn_visualizza)
        input_layout.addWidget(self.btn_cerca)

        main_layout = QHBoxLayout()
        main_layout.addLayout(input_layout)
        main_layout.addWidget(self.result_list)

        central_widget = QWidget()
        central_widget.setLayout(main_layout)

        self.setCentralWidget(central_widget)

        # collegamento dei pulsanti alle funzioni
        self.btn_inserisci.clicked.connect(self.inserisci_dati)
        self.btn_visualizza.clicked.connect(self.visualizza_dati)
        self.btn_cerca.clicked.connect(self.cerca_dati)

    def inserisci_dati(self):
        nome = self.input_nome.text()
        cognome = self.input_cognome.text()

        if nome and cognome:
            self.cursor.execute("INSERT INTO data (nome, cognome) VALUES (?, ?)", (nome, cognome))
            self.conn.commit()
            self.input_nome.clear()
            self.input_cognome.clear()

    def visualizza_dati(self):
        self.result_list.clear()
        self.cursor.execute("SELECT * FROM data")
        data = self.cursor.fetchall()

        for row in data:
            id, nome, cognome = row
            self.result_list.addItem(f"ID: {id}, Nome: {nome}, Cognome: {cognome}")

    def cerca_dati(self):
        nome = self.input_nome.text()
        cognome = self.input_cognome.text()

        if nome or cognome:
            self.result_list.clear()
            query = "SELECT * FROM data WHERE 1"

            params = []

            if nome:
                query += " AND nome = ?"
                params.append(nome)
            if cognome:
                query += " AND cognome = ?"
                params.append(cognome)

            self.cursor.execute(query, params)
            data = self.cursor.fetchall()

            for row in data:
                id, nome, cognome = row
                self.result_list.addItem(f"ID: {id}, Nome: {nome}, Cognome: {cognome}")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = DatabaseApp()
    window.show()
    sys.exit(app.exec_())
