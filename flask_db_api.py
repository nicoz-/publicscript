from flask import Flask, jsonify
import psycopg2

app = Flask(__name__)

# Configurazione del database
db_config = {
    'host': 'localhost',
    'port': '5432',
    'database': 'comuni_italiani',
    'user': 'user',
    'password': 'lololololo'
}

@app.route('/comuni')
def get_comuni():
    # Connessione al database
    conn = psycopg2.connect(**db_config)
    cur = conn.cursor()

    # Esecuzione della query per estrarre i dati
    cur.execute("SELECT comune, cap, prefisso, abitanti FROM comuni c WHERE c.codregione = 'CAM'")

    # Estrazione dei risultati e creazione del JSON di output
    results = cur.fetchall()
    output = []
    for result in results:
        comune = result[0]
        cap = result[1]
        prefisso = result[2]
        abitanti = result[3]
        output.append({
            'comune': comune,
            'cap': cap,
            'prefisso': prefisso,
            'abitanti': abitanti
        })

    # Chiusura della connessione e restituzione del JSON di output
    cur.close()
    conn.close()
    return jsonify(output)

if __name__ == '__main__':
    app.run(debug=True)
