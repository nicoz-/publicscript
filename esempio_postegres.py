import psycopg2

# Connessione al Database
conn = psycopg2.connect(
    host="localhost",
    database="mydatabase",
    user="myusername",
    password="mypassword"
)
cur = conn.cursor()

# Creazione delle tabelle
cur.execute('''CREATE TABLE employee
    (id INT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    age INT NOT NULL,
    salary REAL);''')

cur.execute('''CREATE TABLE department
    (id INT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL);''')

cur.execute('''CREATE TABLE employee_department
    (id SERIAL PRIMARY KEY,
    employee_id INT NOT NULL,
    department_id INT NOT NULL,
    FOREIGN KEY (employee_id)
        REFERENCES employee (id),
    FOREIGN KEY (department_id)
        REFERENCES department (id));''')

cur.execute('''CREATE TABLE project
    (id INT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    start_date DATE,
    end_date DATE);''')

cur.execute('''CREATE TABLE employee_project
    (id SERIAL PRIMARY KEY,
    employee_id INT NOT NULL,
    project_id INT NOT NULL,
    FOREIGN KEY (employee_id)
        REFERENCES employee (id),
    FOREIGN KEY (project_id)
        REFERENCES project (id));''')

# Inserimento di dati
cur.execute("INSERT INTO employee (id, name, age, salary) VALUES (%s, %s, %s, %s)", (1, 'Marco Rossi', 34, 2500))
cur.execute("INSERT INTO department (id, name) VALUES (%s, %s)", (1, 'IT'))
cur.execute("INSERT INTO employee_department (employee_id, department_id) VALUES (%s, %s)", (1, 1))
cur.execute("INSERT INTO project (id, name, start_date, end_date) VALUES (%s, %s, %s, %s)", (1, 'Project X', '2021-01-01', '2021-06-30'))
cur.execute("INSERT INTO employee_project (employee_id, project_id) VALUES (%s, %s)", (1, 1))

# Query di selezione
cur.execute("SELECT employee.name, department.name, project.name FROM employee \
            JOIN employee_department ON employee.id = employee_department.employee_id \
            JOIN department ON department.id = employee_department.department_id \
            JOIN employee_project ON employee.id = employee_project.employee_id \
            JOIN project ON project.id = employee_project.project_id \
            WHERE employee.id = 1")
rows = cur.fetchall()

for row in rows:
    print(row)

# Query di aggiornamento
cur.execute("UPDATE employee SET salary = %s WHERE id = %s", (3000, 1))
conn.commit()

# Chiusura della connessione
cur.close()
conn.close()
