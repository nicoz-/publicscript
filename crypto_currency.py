import sys
import requests
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QComboBox, QPushButton, QVBoxLayout, QWidget, QRadioButton, QHBoxLayout

crypto_dict = {
    'BNB': 'BNBEUR',
    'SOL': 'SOLEUR',
    'DOT': 'DOTEUR',
    'ETH': 'ETHEUR'
}

crypto_us = {
    'LUNA': 'LUNAUSDT',
    'BUSD': 'BUSDUSDT',
    'MAV': 'MAVUSDT'
}

class CryptoPriceApp(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle('Crypto Price Checker')

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout()

        self.label = QLabel('Seleziona una criptovaluta:')
        self.layout.addWidget(self.label)

        self.combo_box = QComboBox()
        self.combo_box.addItems(list(crypto_dict.keys()) + list(crypto_us.keys()))
        self.layout.addWidget(self.combo_box)

        self.currency_label = QLabel('Seleziona la valuta:')
        self.layout.addWidget(self.currency_label)

        self.radio_layout = QHBoxLayout()

        self.usd_radio = QRadioButton('USD')
        self.eur_radio = QRadioButton('EUR')
        self.usd_radio.setChecked(True)

        self.radio_layout.addWidget(self.usd_radio)
        self.radio_layout.addWidget(self.eur_radio)

        self.layout.addLayout(self.radio_layout)

        self.button = QPushButton('Ottieni Valutazione')
        self.button.clicked.connect(self.get_crypto_price)
        self.layout.addWidget(self.button)

        self.result_label = QLabel('')
        self.layout.addWidget(self.result_label)

        self.central_widget.setLayout(self.layout)

    def get_crypto_price(self):
        selected_crypto = self.combo_box.currentText()
        selected_currency = 'USD' if self.usd_radio.isChecked() else 'EUR'

        if selected_crypto in crypto_dict:
            ticker = crypto_dict[selected_crypto]
            currency = 'EUR'
        elif selected_crypto in crypto_us:
            ticker = crypto_us[selected_crypto]
            currency = 'USD'
        else:
            self.result_label.setText(f'Non ho trovato la criptovaluta {selected_crypto}')
            return

        url = f'https://api.binance.com/api/v3/ticker/24hr?symbol={ticker}'
        response = requests.get(url)
        data = response.json()

        if 'lastPrice' in data:
            price = data['lastPrice']
            self.result_label.setText(f'Valutazione attuale di {selected_crypto}: {price} {selected_currency}')
        else:
            self.result_label.setText('Impossibile ottenere la valutazione al momento')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = CryptoPriceApp()
    window.show()
    sys.exit(app.exec_())
