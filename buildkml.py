import simplekml
import glob


coordinates =[]
file_coord = glob.glob("Dati*.txt")[0]
with open(file_coord, 'r') as file_in:
    s = file_in.read().split('\n')
    for coord in s:
        try:
            lat = coord.split('|')[1].replace(',','.')
            long = coord.split('|')[2].replace(',','.')
        
            coordinates =[(lat,long)]
        except IndexError:
            pass

print(coordinates)
kml = simplekml.Kml()
lin = kml.newlinestring(name="Tracciato")
lin.coords = coordinates


kml.save("tracciato.kml")
print('ho salvato il tracciato...')
