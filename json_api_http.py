
import json
from http.server import HTTPServer, BaseHTTPRequestHandler

data = {
    "italy": "spaghetti",
    "germany": "beer",
    "england": "applepie",
    "belgium": "meat"
}

class JSONHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(json.dumps(data).encode())

httpd = HTTPServer(('localhost', 8000), JSONHandler)
httpd.serve_forever()
