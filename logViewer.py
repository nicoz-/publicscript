import sys
import os
import gzip
import subprocess
from PyQt5.QtWidgets import QApplication, QMainWindow, QTreeView, QTextBrowser, QVBoxLayout, QWidget, QFileSystemModel, QSplitter, QPushButton

class LogViewer(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.setWindowTitle('Log Viewer')
        self.setGeometry(100, 100, 800, 600)

        self.splitter = QSplitter(self)
        self.treeView = QTreeView()
        self.textBrowser = QTextBrowser()

        self.splitter.addWidget(self.treeView)
        self.splitter.addWidget(self.textBrowser)

        self.centralWidget = QWidget(self)
        self.setCentralWidget(self.centralWidget)

        self.layout = QVBoxLayout(self.centralWidget)
        self.layout.addWidget(self.splitter)

        self.refreshButton = QPushButton('Refresh', self)
        self.layout.addWidget(self.refreshButton)
        self.refreshButton.clicked.connect(self.refreshLogs)

        self.fileSystemModel = QFileSystemModel()
        self.fileSystemModel.setRootPath('/var/log/')  # Change this to the desired directory

        self.treeView.setModel(self.fileSystemModel)
        self.treeView.setRootIndex(self.fileSystemModel.index('/var/log/'))  # Change this to the desired directory

        self.treeView.clicked.connect(self.loadLog)

    def loadLog(self, index):
        file_path = self.fileSystemModel.filePath(index)
        if os.path.isfile(file_path):
            if file_path.endswith('.gz'):
                with gzip.open(file_path, 'rt', encoding='utf-8') as f:
                    log_content = f.read()
                    self.textBrowser.setPlainText(log_content)
            else:
                with open(file_path, 'r', encoding='utf-8', errors='replace') as f:
                    log_content = f.read()
                    self.textBrowser.setPlainText(log_content)

    def refreshLogs(self):
        self.fileSystemModel.setRootPath('/var/log/')
        self.treeView.setRootIndex(self.fileSystemModel.index('/var/log/'))

# ADD OTHER FUNCTIONS

if __name__ == '__main__':
    app = QApplication(sys.argv)

    viewer = LogViewer()
    viewer.show()

    sys.exit(app.exec_())
