import sys
import os
import json
from PyQt5.QtCore import QDate, Qt, QEvent
from PyQt5.QtWidgets import QApplication, QMainWindow, QCalendarWidget, QTextEdit, QLabel, QVBoxLayout, QWidget, QPushButton, QLineEdit, QFileDialog, QMessageBox

class CalendarApp(QMainWindow):
    def __init__(self):
        super().__init__()

        self.notes_file = "notes.json"
        self.data = self.load_data()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('Calendario con Note, Spese ed Entrate by nicoz')

        self.calendar = QCalendarWidget(self)
        self.calendar.setGridVisible(True)
        self.calendar.clicked[QDate].connect(self.show_notes)

        self.notes_edit = QTextEdit(self)
        self.notes_edit.setPlaceholderText('Inserisci note qui...')

        self.expenses_edit = QLineEdit(self)
        self.expenses_edit.setPlaceholderText('Spese giornaliere...')
        self.expenses_edit.returnPressed.connect(self.save_expenses)

        self.income_edit = QLineEdit(self)
        self.income_edit.setPlaceholderText('Entrate mensili...')
        self.income_edit.returnPressed.connect(self.save_income)

        self.total_label = QLabel(self)

        self.save_button = QPushButton('Salva', self)
        self.save_button.clicked.connect(self.save_data)

        self.load_button = QPushButton('Carica', self)
        self.load_button.clicked.connect(self.load_data)

        layout = QVBoxLayout()
        layout.addWidget(self.calendar)
        layout.addWidget(self.notes_edit)
        layout.addWidget(self.expenses_edit)
        layout.addWidget(self.income_edit)
        layout.addWidget(self.total_label)
        layout.addWidget(self.save_button)
        layout.addWidget(self.load_button)

        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)

        self.show()

    def show_notes(self, date):
        selected_date = date.toString("yyyy-MM-dd")

        if self.data and selected_date in self.data:
            self.notes_edit.setPlainText(self.data[selected_date].get('notes', ''))
            self.expenses_edit.setText(str(self.data[selected_date].get('expenses', '')))

        self.calculate_total()

    def save_expenses(self):
        selected_date = self.calendar.selectedDate().toString("yyyy-MM-dd")
        expenses = self.expenses_edit.text()

        if expenses:
            try:
                amount = float(expenses)
                if selected_date in self.data:
                    self.data[selected_date]['expenses'] = amount
                else:
                    self.data[selected_date] = {'expenses': amount}
            except ValueError:
                pass

        self.expenses_edit.clear()
        self.calculate_total()

    def save_income(self):
        income = self.income_edit.text()
        if income:
            self.data['income'] = float(income)
        else:
            self.data['income'] = 0
        self.calculate_total()

    def save_data(self):
        selected_date = self.calendar.selectedDate().toString("yyyy-MM-dd")
        note_text = self.notes_edit.toPlainText()

        if selected_date in self.data:
            self.data[selected_date]['notes'] = note_text
        else:
            self.data[selected_date] = {'notes': note_text}

        self.save_data_to_file()

    def calculate_total(self):
        total_expenses = sum(entry.get('expenses', 0) for entry in self.data.values() if isinstance(entry, dict))
        income = self.data.get('income', 0)
        balance = income - total_expenses

        self.total_label.setText(f'Totale mensile:\nEntrate: {income:.2f}\nUscite giornaliere: {total_expenses:.2f}\nBilancio: {balance:.2f}')

    def load_data(self):
        if os.path.exists(self.notes_file):
            with open(self.notes_file, 'r') as f:
                try:
                    data = json.load(f)
                    return data
                except json.JSONDecodeError:
                    return {}
        else:
            return {}

    def save_data_to_file(self):
        with open(self.notes_file, 'w') as file:
            json.dump(self.data, file, indent=4)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = CalendarApp()
    sys.exit(app.exec_())
