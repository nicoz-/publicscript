import cv2
import subprocess

# Definisci la funzione che esegue un comando nella shell
def execute_command(command):
    subprocess.Popen(command, shell=True)

# Carica il classificatore Haar per la rilevazione delle mani
hand_cascade = cv2.CascadeClassifier('/home/nicoz/Desktop/PYTHON/haarcascade_hand.xml')

# Avvia la telecamera
cap = cv2.VideoCapture(0)

while True:
    # Leggi un frame dalla telecamera
    ret, frame = cap.read()

    # Converti il frame in scala di grigi
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Rileva le mani nel frame
    hands = hand_cascade.detectMultiScale(gray, 1.2, 4)

    # Disegna un rettangolo intorno alle mani
    for (x, y, w, h) in hands:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    # Controlla se il gesto OK delle mani è stato rilevato
    ok_gesture_detected = False
    for (x, y, w, h) in hands:
        if w > 0.7 * h:
            ok_gesture_detected = True
            break

    # Se il gesto OK delle mani è stato rilevato, esegui il comando
    if ok_gesture_detected:
        print('OK')
        execute_command('ls')  # esempio di comando da eseguire

    # Mostra il frame con i rettangoli intorno alle mani
    cv2.imshow('frame', frame)

    # Se premi il tasto ESC, esci dal ciclo
    if cv2.waitKey(1) == 27:
        break

# Chiudi la finestra e rilascia la telecamera
cap.release()
cv2.destroyAllWindows()
