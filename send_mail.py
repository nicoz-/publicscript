import sys
import smtplib
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QTextEdit, QPushButton, QVBoxLayout, QWidget, QMessageBox

class EmailApp(QMainWindow):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Applicazione per l'invio di email")
        self.setGeometry(100, 100, 400, 300)

        layout = QVBoxLayout()

        self.lbl_to = QLabel("Destinatario:")
        self.txt_to = QLineEdit()
        layout.addWidget(self.lbl_to)
        layout.addWidget(self.txt_to)

        self.lbl_subject = QLabel("Oggetto:")
        self.txt_subject = QLineEdit()
        layout.addWidget(self.lbl_subject)
        layout.addWidget(self.txt_subject)

        self.lbl_message = QLabel("Messaggio:")
        self.txt_message = QTextEdit()
        layout.addWidget(self.lbl_message)
        layout.addWidget(self.txt_message)

        self.btn_send = QPushButton("Invia")
        self.btn_send.clicked.connect(self.send_email)
        layout.addWidget(self.btn_send)

        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)

    def send_email(self):
        to_email = self.txt_to.text()
        subject = self.txt_subject.text()
        message = self.txt_message.toPlainText()

        smtp_server = "smtp.gmail.com"
        smtp_port = 465  #FOR SSL/TLS 
        smtp_username = ""
        smtp_password = ""

        try:
            server = smtplib.SMTP_SSL(smtp_server, smtp_port)
            server.login(smtp_username, smtp_password)

            msg = f"Subject: {subject}\n\n{message}"
            server.sendmail(smtp_username, to_email, msg)

            server.quit()

            QMessageBox.information(self, "Successo", "Email inviata con successo!")
        except Exception as e:
            QMessageBox.critical(self, "Errore", f"Errore durante l'invio dell'email:\n{str(e)}")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = EmailApp()
    window.show()
    sys.exit(app.exec_())
